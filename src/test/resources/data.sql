INSERT INTO "role"(id, name) VALUES (1, 'User');
INSERT INTO "role"(id, name) VALUES (2, 'Admin');

INSERT INTO "user"(created_date, email, status, name, surname, username, password)
VALUES(now(), 'EricCartman@gmail.com', 'ACTIVE', 'Eric', 'Cartman', 'EricCartman', '$2a$10$mCy.W6cos5hwlOz4DMBqG.17kyjxhBSrikd4mYYJrrhRCCGmsALqm');
INSERT INTO "user"(created_date, email, status, name, surname, username, password)
VALUES(now(), 'StanlyMarch@gmail.com', 'ACTIVE', 'Stanly', 'March', 'StanlyMarch', '$2a$10$WAdZPM19p2UyxzZpjFchwePmdCSPmPDLdPpmKIwTvAFHclExapN0.');
INSERT INTO "user"(created_date, email, status, name, surname, username, password)
VALUES(now(), 'KyleBroflovski@gmail.com', 'ACTIVE', 'Kyle', 'Broflovski', 'KyleBroflovski', '$2a$10$pPkSATV1IaZvcC6fbpUIzO9cieOYjd4jDFxtoGXijFwL1lhcsss7y');
INSERT INTO "user"(created_date, email, status, name, surname, username, password)
VALUES(now(), 'KennyMcCormick@gmail.com', 'ACTIVE', 'Kenny', 'McCormick', 'KennyMcCormick', '$2a$10$0vU2xGseVESwcGPJW8qybeOXMb1nOjgs0xCQy8YjufYzXo567UWLm');
INSERT INTO "user"(created_date, email, status, name, surname, username, password)
VALUES(now(), 'StanleyMarsh@gmail.com', 'ACTIVE', 'Stanley', 'Marsh', 'StanleyMarsh', '$2a$10$hJaB8EFTZtgSniTmb7tqTOAoeAGq3V4Ff/wtffalBkmlER10GzNbC');

INSERT INTO user_role(user_id, role_id)
VALUES(1, 1);
INSERT INTO user_role(user_id, role_id)
VALUES(2, 1);
INSERT INTO user_role(user_id, role_id)
VALUES(3, 1);
INSERT INTO user_role(user_id, role_id)
VALUES(4, 1);
INSERT INTO user_role(user_id, role_id)
VALUES(5, 1);
