package ru.iskhakov.uchatserver.controller.mapper;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class BaseMapperTest {

    @Test
    void mapToDto() {
    }

    @Test
    void mapToEntity() {
    }

    @Test
    void mapToDtos() {
    }

    @Test
    void mapToEntities() {
    }
}