package ru.iskhakov.uchatserver.controller;

import org.junit.jupiter.api.Test;
import ru.iskhakov.uchatserver.controller.config.ControllerTestConfig;

import static org.junit.jupiter.api.Assertions.*;

class UserControllerTest extends ControllerTestConfig {
    @Test
    void update() {
    }

    @Test
    void delete() {
    }

    @Test
    void get() {
    }

    @Test
    void getByUsername() {
    }

    @Test
    void getByEmail() {
    }

    @Test
    void getAll() {
    }

    @Test
    void getContacts() {
    }

    @Test
    void addContact() {
    }
}
