package ru.iskhakov.uchatserver.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MessageServiceBeanTest {

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void create() {
    }

    @Test
    void update() {
    }

    @Test
    void find() {
    }

    @Test
    void findAll() {
    }

    @Test
    void delete() {
    }

    @Test
    void deleteAll() {
    }

    @Test
    void findBySender() {
    }

    @Test
    void findMessages() {
    }

    @Test
    void findBySenderAndRecipients() {
    }

    @Test
    void findByCreatedDateAfter() {
    }

    @Test
    void findByCreatedDateAfterAndSender() {
    }

    @Test
    void sendConnectMessage() {
    }

    @Test
    void sendDisconnectMessage() {
    }
}