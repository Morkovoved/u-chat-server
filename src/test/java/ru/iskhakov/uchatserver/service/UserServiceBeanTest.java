package ru.iskhakov.uchatserver.service;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class UserServiceBeanTest {

    @Test
    void create() {
    }

    @Test
    void update() {
    }

    @Test
    void find() {
    }

    @Test
    void delete() {
    }

    @Test
    void deleteAll() {
    }

    @Test
    void existsByUsername() {
    }

    @Test
    void existsByEmail() {
    }

    @Test
    void findByUsername() {
    }

    @Test
    void findByEmail() {
    }

    @Test
    void findAll() {
    }

    @Test
    void findByName() {
    }

    @Test
    void findByNameLike() {
    }

    @Test
    void findBySurname() {
    }

    @Test
    void findByNameAndSurname() {
    }

    @Test
    void findByEnabled() {
    }

    @Test
    void findByCreatedDateAfter() {
    }

    @Test
    void findContacts() {
    }

    @Test
    void addContact() {
    }
}