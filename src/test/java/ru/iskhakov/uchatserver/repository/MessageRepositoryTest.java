package ru.iskhakov.uchatserver.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import ru.iskhakov.uchatserver.model.Message;
import ru.iskhakov.uchatserver.model.User;
import ru.iskhakov.uchatserver.repository.config.RepositoryTestConfig;
import ru.iskhakov.uchatserver.util.TestUtils;

import java.time.LocalDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class MessageRepositoryTest extends RepositoryTestConfig {
    @Autowired
    MessageRepository messageRepository;

    @Test
    void save() {
        Message message = TestUtils.createRandomMessageToSave();
        Long id = message.getId();
        String senderName = message.getSender().getName();
        String typeName = message.getType().name();
        String statusName = message.getStatus().name();
        LocalDateTime createdDate = message.getCreatedDate();
        Message savedMessage = messageRepository.save(message);
        assertEquals(message, savedMessage);
        assertEquals(senderName, savedMessage.getSender().getName());
        assertEquals(typeName, savedMessage.getType().name());
        assertEquals(statusName, savedMessage.getStatus().name());
        assertEquals(typeName, savedMessage.getType().name());
        assertEquals(createdDate, savedMessage.getCreatedDate());
        assertNotNull(savedMessage.getId());
        assertNotEquals(id, savedMessage.getId());

    }

    @Test
    void saveAll() {
    }

    @Test
    void findById() {
    }

    @Test
    void existsById() {
    }

    @Test
    void findAll() {
    }

    @Test
    void findAllById() {
    }

    @Test
    void deleteById() {
    }

    @Test
    void delete() {
    }

    @Test
    void deleteAll() {
    }

    @Test
    void findBySender() {
    }

    @Test
    void findBySenderIdAndRecipientsId() {
    }

    @Test
    void findByCreatedDateAfter() {
    }

    @Test
    void findByCreatedDateAfterAndSender() {
    }

    @Test
    void findMessages() {
    }
}