package ru.iskhakov.uchatserver.repository;

import org.junit.jupiter.api.Test;
import ru.iskhakov.uchatserver.model.User;

import static org.junit.jupiter.api.Assertions.*;

class UserRepositoryTest {

    @Test
    void save() {
    }

    @Test
    void saveAll() {
    }

    @Test
    void findById() {
    }

    @Test
    void existsById() {
    }

    @Test
    void findAll() {
    }

    @Test
    void findAllById() {
    }

    @Test
    void deleteById() {
    }

    @Test
    void delete() {
    }

    @Test
    void deleteAll() {
    }

    @Test
    void existsByUsername() {
    }

    @Test
    void existsByEmail() {
    }

    @Test
    void findByUsername() {
    }

    @Test
    void findByEmail() {
    }

    @Test
    void findByName() {
    }

    @Test
    void findByNameLike() {
    }

    @Test
    void findBySurname() {
    }

    @Test
    void findByNameAndSurname() {
    }

    @Test
    void findByEnabled() {
    }

    @Test
    void findByCreatedDateAfter() {
    }
}