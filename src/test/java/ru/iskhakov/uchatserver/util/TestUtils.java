package ru.iskhakov.uchatserver.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import ru.iskhakov.uchatserver.model.Message;
import ru.iskhakov.uchatserver.model.enums.MessageType;
import ru.iskhakov.uchatserver.model.Role;
import ru.iskhakov.uchatserver.model.User;
import ru.iskhakov.uchatserver.model.enums.Status;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.apache.commons.lang3.RandomUtils.nextLong;

public class TestUtils {

    public static Message createRandomMessage() {
        MessageType messageType = MessageType.values()[nextInt(0, MessageType.values().length)];
        Status status = Status.values()[nextInt(0, Status.values().length)];
        Message message = new Message();
        message.setId(nextLong(0, 100_000L));
        message.setType(messageType);
        message.setContent(randomAlphabetic(20));
        message.setCreatedDate(LocalDateTime.now());
        message.setUpdatedDate(LocalDateTime.now());
        message.setStatus(status);
        message.setSender(createUser());
        message.setRecipients(Arrays.asList(createUser(), createUser()));
        return message;
    }

    public static Message createRandomMessageToSave() {
        Message message = createRandomMessage();
        message.setId(null);
        return message;
    }

    public static Message createChatMessage() {
        Message message = createRandomMessage();
        message.setType(MessageType.CHAT);
        return message;
    }


    public static Role createRole() {
        Role role = new Role();
        role.setId(1);
        role.setName("User");
        return role;
    }

    public static User createUser() {
        Role role = createRole();

        User user = new User();
        user.setName(randomAlphabetic(20));
        user.setSurname(randomAlphabetic(20));
        user.setEmail(randomAlphabetic(20) + "@gmail.com");
        user.setUsername(randomAlphabetic(20));
        user.setPassword(randomAlphabetic(20));
        user.setRoles(Collections.singleton(role));
        return user;
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
