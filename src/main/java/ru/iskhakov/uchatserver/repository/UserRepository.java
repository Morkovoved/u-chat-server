package ru.iskhakov.uchatserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.iskhakov.uchatserver.model.User;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Integer> {
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
    Optional<User> findByUsername(String username);
    Optional<User> findByEmail(String email);
    List<User> findByName(String name);
    List<User> findByNameLike(String name);
    List<User> findBySurname(String surname);
    List<User> findByNameAndSurname(String name, String surname);
    List<User> findByStatus(String status);
    List<User> findByCreatedDateAfter(LocalDateTime startDate);
}

