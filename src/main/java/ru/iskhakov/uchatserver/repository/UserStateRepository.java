package ru.iskhakov.uchatserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.iskhakov.uchatserver.model.UserState;

public interface UserStateRepository extends JpaRepository<UserState, Integer> {
}
