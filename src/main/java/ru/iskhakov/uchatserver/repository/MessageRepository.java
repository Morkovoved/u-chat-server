package ru.iskhakov.uchatserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import ru.iskhakov.uchatserver.model.Message;

import java.time.LocalDateTime;
import java.util.List;

public interface MessageRepository extends JpaRepository<Message, Long> {
    List<Message> findBySender(Integer id);
    List<Message> findBySenderIdAndRecipientsId(Integer senderId, Integer recipientId);
    List<Message> findByCreatedDateAfter(LocalDateTime startDate);
    List<Message> findByCreatedDateAfterAndSender(LocalDateTime startDate, Integer id);
    @Query(value = "SELECT m FROM Message m " +
            "  JOIN m.recipients r " +
            "  WHERE m.sender.id IN (:senderId, :recipientId) " +
            "    AND r.id IN (:senderId, :recipientId)")
    List<Message> findMessages(@Param("senderId")Integer senderId, @Param("recipientId")Integer recipientId);
}
