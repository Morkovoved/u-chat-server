package ru.iskhakov.uchatserver.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.iskhakov.uchatserver.model.Role;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(String name);
}
