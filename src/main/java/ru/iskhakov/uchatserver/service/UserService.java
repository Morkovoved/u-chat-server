package ru.iskhakov.uchatserver.service;

import ru.iskhakov.uchatserver.model.User;

import java.time.LocalDateTime;
import java.util.List;

public interface UserService extends CrudService<User, Integer> {
    Boolean existsByUsername(String username);
    Boolean existsByEmail(String email);
    User findByUsername(String username);
    User findByEmail(String email);
    List<User> findAll();
    List<User> findByName(String name);
    List<User> findByNameLike(String name);
    List<User> findBySurname(String surname);
    List<User> findByNameAndSurname(String name, String surname);
    List<User> findByStatus(String status);
    List<User> findByCreatedDateAfter(LocalDateTime startDate);
    List<User> findContacts(Integer id);
    User addContact(Integer id, Integer contactId);
}

