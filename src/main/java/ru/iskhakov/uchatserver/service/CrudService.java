package ru.iskhakov.uchatserver.service;

import java.util.List;

public interface CrudService <T, ID> {
    T create(T object);
    T update(T object);
    T find(ID id);
    List<T> findAll();
    void delete(ID id);
    void deleteAll();
}
