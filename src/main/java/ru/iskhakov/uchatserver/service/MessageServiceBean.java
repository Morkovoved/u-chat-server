package ru.iskhakov.uchatserver.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iskhakov.uchatserver.controller.dto.MessageDto;
import ru.iskhakov.uchatserver.controller.mapper.MessageMapper;
import ru.iskhakov.uchatserver.exception.MessageNotFoundException;
import ru.iskhakov.uchatserver.model.*;
import ru.iskhakov.uchatserver.model.enums.MessageType;
import ru.iskhakov.uchatserver.model.enums.UserStateType;
import ru.iskhakov.uchatserver.repository.MessageRepository;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class MessageServiceBean implements MessageService {
    private final MessageRepository messageRepository;
    private final UserService userService;
    private final UserStateService userStateService;
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final MessageMapper messageMapper;

    @Override
    @Transactional
    public Message create(Message message) {
        return messageRepository.save(message);
    }

    @Override
    public Message update(Message message) {
        return messageRepository.save(message);
    }

    @Override
    public Message find(Long id) {
        return messageRepository.findById(id).orElseThrow(MessageNotFoundException::new);
    }

    @Override
    public List<Message> findAll() {
        return messageRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        messageRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        messageRepository.deleteAll();
    }

    @Override
    public List<Message> findBySender(Integer id) {
        return messageRepository.findBySender(id);
    }

    @Override
    public List<Message> findMessages(Integer senderId, Integer recipientId) {
        return messageRepository.findMessages(senderId, recipientId);
    }

    @Override
    public List<Message> findBySenderAndRecipients(Integer id, Integer contactId) {
        return messageRepository.findBySenderIdAndRecipientsId(id, contactId);
    }

    @Override
    public List<Message> findByCreatedDateAfter(LocalDateTime startDate) {
        return messageRepository.findByCreatedDateAfter(startDate);
    }

    @Override
    public List<Message> findByCreatedDateAfterAndSender(LocalDateTime startDate, Integer id) {
        return messageRepository.findByCreatedDateAfterAndSender(startDate, id);
    }

    @Override
    @Transactional
    public void sendConnectMessage(String username) {
        String content = "User connected : " + username;
        sendWebsocketEventMessage(username, MessageType.JOIN, content);
    }

    @Override
    @Transactional
    public void sendDisconnectMessage(String username) {
        String content = "User disconnected : " + username;
        sendWebsocketEventMessage(username, MessageType.LEAVE, content);
    }

    private void sendWebsocketEventMessage(String username, MessageType messageType, String content) {
        User user = userService.findByUsername(username);
        UserState userState = new UserState(UserStateType.ONLINE, user);
        userStateService.create(userState);
        user.setUserState(userState);

        Message message = new Message();
        message.setType(messageType);
        message.setSender(user);
        message.setContent(content);
//        message.setRecipients(user.getContacts());
        create(message);

        MessageDto messageDto = messageMapper.mapToDto(message);
        for(User recipient: user.getContacts()) {
            simpMessagingTemplate.convertAndSendToUser(recipient.getUsername(), "/queue/message/private", messageDto);
        }
    }
}
