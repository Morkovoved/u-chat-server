package ru.iskhakov.uchatserver.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.iskhakov.uchatserver.exception.NotFoundException;
import ru.iskhakov.uchatserver.model.UserState;
import ru.iskhakov.uchatserver.repository.UserStateRepository;

import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserStateServiceBean implements UserStateService {
    private final UserStateRepository userStateRepository;

    @Override
    public UserState create(UserState userState) {
        return userStateRepository.save(userState);
    }

    @Override
    public UserState update(UserState userState) {
        return userStateRepository.save(userState);
    }

    @Override
    public UserState find(Integer id) {
        return userStateRepository.findById(id).orElseThrow(NotFoundException::new);
    }

    @Override
    public List<UserState> findAll() {
        return userStateRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        userStateRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {

    }
}
