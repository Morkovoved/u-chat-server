package ru.iskhakov.uchatserver.service;

import ru.iskhakov.uchatserver.model.Message;

import java.time.LocalDateTime;
import java.util.List;

public interface MessageService extends CrudService<Message, Long> {
    List<Message> findBySender(Integer id);
    List<Message> findMessages(Integer senderId, Integer recipientId);
    List<Message> findBySenderAndRecipients(Integer senderId, Integer recipientId);
    List<Message> findByCreatedDateAfter(LocalDateTime startDate);
    List<Message> findByCreatedDateAfterAndSender(LocalDateTime startDate, Integer id);
    void sendConnectMessage(String username);
    void sendDisconnectMessage(String username);
}
