package ru.iskhakov.uchatserver.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.iskhakov.uchatserver.exception.NotFoundException;
import ru.iskhakov.uchatserver.exception.UserNotFoundException;
import ru.iskhakov.uchatserver.model.Role;
import ru.iskhakov.uchatserver.model.User;
import ru.iskhakov.uchatserver.model.enums.Status;
import ru.iskhakov.uchatserver.repository.RoleRepository;
import ru.iskhakov.uchatserver.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Service
@Slf4j
@RequiredArgsConstructor
//@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserServiceBean implements UserService {
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;

    @Transactional
    @Override
    public User create(User user) {
        if (user.getId() != null && !userRepository.existsById(user.getId())) {
            throw new UserNotFoundException();
        }

        Role role = roleRepository.findByName("User").orElseThrow(NotFoundException::new);

        user.setRoles(Collections.singleton(role));
        user.setCreatedDate(LocalDateTime.now());
        user.setStatus(Status.ACTIVE);
        return userRepository.save(user);
    }

    @Override
    public User update(User user) {
        return userRepository.save(user);
    }

    @Override
    public User find(Integer id) {
        return userRepository.findById(id).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public void delete(Integer id) {
        userRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        userRepository.deleteAll();
    }

    @Override
    public Boolean existsByUsername(String username) {
        return userRepository.existsByUsername(username);
    }

    @Override
    public Boolean existsByEmail(String email) {
        return userRepository.existsByEmail(email);
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByUsername(email).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public List<User> findByName(String name) {
        return userRepository.findByName(name);
    }

    @Override
    public List<User> findByNameLike(String name) {
        return userRepository.findByNameLike(name);
    }

    @Override
    public List<User> findBySurname(String surname) {
        return userRepository.findBySurname(surname);
    }

    @Override
    public List<User> findByNameAndSurname(String name, String surname) {
        return userRepository.findByNameAndSurname(name, surname);
    }

    @Override
    public List<User> findByStatus(String status) {
        return userRepository.findByStatus(status);
    }

    @Override
    public List<User> findByCreatedDateAfter(LocalDateTime startDate) {
        return userRepository.findByCreatedDateAfter(startDate);
    }

    @Override
    public List<User> findContacts(Integer id) {
        return userRepository.findById(id).orElseThrow(UserNotFoundException::new).getContacts();
    }

    @Override
//    @Transactional
    public User addContact(Integer id, Integer contactId) {
        User user = find(id);
        User contact = find(contactId);
        user.getContacts().add(contact);
        userRepository.save(user);
        return user;
    }
}
