package ru.iskhakov.uchatserver.service;

import ru.iskhakov.uchatserver.model.UserState;

public interface UserStateService extends CrudService<UserState, Integer> {
}
