package ru.iskhakov.uchatserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UChatServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(UChatServerApplication.class, args);
	}

}
