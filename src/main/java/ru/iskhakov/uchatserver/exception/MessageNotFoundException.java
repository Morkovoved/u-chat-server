package ru.iskhakov.uchatserver.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class MessageNotFoundException extends RuntimeException {
    public MessageNotFoundException() {
        super();
    }
    public MessageNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
    public MessageNotFoundException(String message) {
        super(message);
    }
    public MessageNotFoundException(Throwable cause) {
        super(cause);
    }
}
