package ru.iskhakov.uchatserver.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.iskhakov.uchatserver.model.enums.UserStateType;

import javax.persistence.*;
import java.util.List;
import java.util.Set;

@Data
@Entity
@Table(name = "\"user\"")
@EqualsAndHashCode(exclude = {"contacts", "userState"})
@ToString(exclude = {"contacts"})
public class User extends BaseEntity<Integer> {
    private String username;
    private String password;
    private String name;
    private String surname;
    private String email;
    @Transient
    private UserState userState = new UserState(UserStateType.OFFLINE, this);
    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Set<Role> roles;
    @OneToMany(cascade = CascadeType.DETACH, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinTable(name="user_contact",
            joinColumns = { @JoinColumn(name="user_id", referencedColumnName="id") },
            inverseJoinColumns = { @JoinColumn(name="contact_id", referencedColumnName="id", unique=true) })
    private List<User> contacts;
}
