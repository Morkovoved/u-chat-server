package ru.iskhakov.uchatserver.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.iskhakov.uchatserver.model.enums.MessageType;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@Table(name = "message")
@EqualsAndHashCode(exclude = {"recipients"}, callSuper = true)
@ToString(exclude = {"recipients"}, callSuper = true)
public class Message extends BaseEntity<Long> {
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "sender_id", referencedColumnName = "id")
    private User sender;
    @ManyToMany
    @JoinTable(name = "message_recipient",
            joinColumns = @JoinColumn(name = "message_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"))
    private List<User> recipients;
    @Enumerated(EnumType.STRING)
    private MessageType type;
    @Lob
    private String content;
}
