package ru.iskhakov.uchatserver.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ru.iskhakov.uchatserver.model.enums.UserStateType;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
//@Table(name = "user_state")
@ToString(exclude = {"user"})
@NoArgsConstructor
public class UserState extends BaseEntity<Integer> {

    @Enumerated(EnumType.STRING)
    private UserStateType userStateType;
    @ManyToOne
    @JoinColumn(name="user_id")
    private User user;

    public UserState(UserStateType userStateType, User user) {
        this.userStateType = userStateType;
        this.user = user;
        this.createdDate = LocalDateTime.now();
        this.updatedDate = LocalDateTime.now();
    }
}
