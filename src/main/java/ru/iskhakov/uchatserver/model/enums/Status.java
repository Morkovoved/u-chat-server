package ru.iskhakov.uchatserver.model.enums;

public enum Status {
    ACTIVE,
    NOT_ACTIVE,
    DELETED
}
