package ru.iskhakov.uchatserver.model.enums;

public enum UserStateType {
    ONLINE,
    OFFLINE,
    BUSY
}
