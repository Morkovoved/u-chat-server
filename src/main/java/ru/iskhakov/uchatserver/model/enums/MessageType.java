package ru.iskhakov.uchatserver.model.enums;

public enum MessageType {
    CHAT,
    JOIN,
    LEAVE
}
