package ru.iskhakov.uchatserver.model;

import lombok.Data;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import ru.iskhakov.uchatserver.model.enums.Status;

import javax.persistence.*;
import java.time.LocalDateTime;

@MappedSuperclass
@Data
public class BaseEntity<T extends Number> {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    protected T id;
    @CreatedDate
    protected LocalDateTime createdDate;
    @LastModifiedDate
    protected LocalDateTime updatedDate;
    @Enumerated(EnumType.STRING)
    protected Status status;
}
