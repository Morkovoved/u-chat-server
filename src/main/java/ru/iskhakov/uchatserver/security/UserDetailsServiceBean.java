package ru.iskhakov.uchatserver.security;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.iskhakov.uchatserver.model.Role;
import ru.iskhakov.uchatserver.model.enums.Status;
import ru.iskhakov.uchatserver.repository.UserRepository;

import java.util.stream.Collectors;

@Service
public class UserDetailsServiceBean implements UserDetailsService {
    private UserRepository userRepository;

    public UserDetailsServiceBean(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository
                .findByUsername(username)
                .map(user -> org.springframework.security.core.userdetails.User.builder()
                        .username(user.getUsername())
                        .password(user.getPassword())
                        .accountLocked(Status.NOT_ACTIVE.equals(user.getStatus()))
                        .disabled(Status.DELETED.equals(user.getStatus()))
                        .roles(user.getRoles().stream()
                                .map(Role::getName)
                                .toArray(String[]::new)
                        )
                        .build())
                .orElseThrow(() -> new UsernameNotFoundException("No user with the name " + username + "was found in the database"));
    }

}
