package ru.iskhakov.uchatserver.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.iskhakov.uchatserver.controller.dto.MessageDto;
import ru.iskhakov.uchatserver.controller.mapper.MessageMapper;
import ru.iskhakov.uchatserver.model.Message;
import ru.iskhakov.uchatserver.service.MessageService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(value = "/messages", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
@Slf4j
@RequiredArgsConstructor
public class MessageController {
    private final MessageService messageService;
    private final MessageMapper messageMapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public MessageDto create(@Valid @RequestBody MessageDto messageDto) {
        Message message = messageService.create(messageMapper.mapToEntity(messageDto));
        return messageMapper.mapToDto(message);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        messageService.delete(id);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public MessageDto get(@PathVariable Long id) {
        Message message = messageService.find(id);
        return messageMapper.mapToDto(message);
    }

    @GetMapping("/sender/{senderId}/recipient/{recipientId}")
    @ResponseStatus(HttpStatus.OK)
    public List<MessageDto> get(@PathVariable Integer senderId, @PathVariable Integer recipientId) {
        List<Message> messages = messageService.findMessages(senderId, recipientId);
        return messageMapper.mapToDtos(messages);
    }

    @GetMapping("/search")
    @ResponseStatus(HttpStatus.OK)
    public List<MessageDto> searchBySender(@RequestParam("id") Integer id) {
        List<Message> messages =  messageService.findBySender(id);
        return messageMapper.mapToDtos(messages);
    }
}
