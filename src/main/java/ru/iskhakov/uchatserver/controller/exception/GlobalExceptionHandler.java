package ru.iskhakov.uchatserver.controller.exception;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.iskhakov.uchatserver.exception.*;

import javax.persistence.EntityNotFoundException;
import javax.validation.ConstraintViolationException;
import java.io.FileNotFoundException;

@RestControllerAdvice
@Slf4j
@RequiredArgsConstructor
public class GlobalExceptionHandler {

    @ExceptionHandler(BadCredentialsException.class)
    public ErrorDto handleBadCredentialsException(final BadCredentialsException e) {
        return new ErrorDto(e, null, HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(DataAccessException.class)
    public ErrorDto handleDataAccessException(final DataAccessException e) {
        return new ErrorDto(e, null, HttpStatus.INTERNAL_SERVER_ERROR);
    }


    @ExceptionHandler(IllegalArgumentException.class)
    public ErrorDto handleIllegalArgumentException(final IllegalArgumentException e) {
        return new ErrorDto(e, null, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(FileUploadException.class)
    public ErrorDto handleFileUploadException(final FileUploadException e) {
        return new ErrorDto(e, null, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(FileNotFoundException.class)
    public ErrorDto handleFileNotFoundException(final FileNotFoundException e) {
        return new ErrorDto(e, null, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public ErrorDto handleEntityNotFoundException(final EntityNotFoundException e) {
        return new ErrorDto(e, null, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    public ErrorDto handleConstraintViolationException(final ConstraintViolationException e) {
        final StringBuilder builder = new StringBuilder();
        e.getConstraintViolations().forEach(violation -> builder.append(
                String.format("%s Current value: %s", violation.getMessage(), violation.getInvalidValue())));
        return new ErrorDto(e, builder.toString(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MessageNotFoundException.class)
    public ErrorDto handleMessageNotFoundException(final MessageNotFoundException e) {
        return new ErrorDto(e, null, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NotFoundException.class)
    public ErrorDto handleNotFoundException(final NotFoundException e) {
        return new ErrorDto(e, null, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UserNotFoundException.class)
    public ErrorDto handleUserNotFoundException(final UserNotFoundException e) {
        return new ErrorDto(e, null, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(UserAlreadyExistException.class)
    public ErrorDto handleUserAlreadyExistException(final UserAlreadyExistException e) {
        return new ErrorDto(e, null, HttpStatus.CONFLICT);
    }

    @ExceptionHandler(NotAuthorizedException.class)
    public ErrorDto handleNotAuthorizedException(final NotAuthorizedException e) {
        return new ErrorDto(e, null, HttpStatus.UNAUTHORIZED);
    }
}
