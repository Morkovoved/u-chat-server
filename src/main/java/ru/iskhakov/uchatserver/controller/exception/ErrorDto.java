package ru.iskhakov.uchatserver.controller.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.http.HttpStatus;

import java.time.LocalDateTime;

@Data
public class ErrorDto {
    private HttpStatus status;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy hh:mm:ss")
    private LocalDateTime timestamp;
    private String message;
    private String debugMessage;

    private ErrorDto() {
        timestamp = LocalDateTime.now();
    }

    ErrorDto(HttpStatus status) {
        this();
        this.status = status;
    }

    ErrorDto(Throwable throwable, HttpStatus status) {
        this();
        this.status = status;
        this.message = "Unexpected error";
        this.debugMessage = throwable.getLocalizedMessage();
    }

    ErrorDto(Throwable throwable, String message, HttpStatus status) {
        this();
        this.status = status;
        this.message = message;
        this.debugMessage = throwable.getLocalizedMessage();
    }
}
