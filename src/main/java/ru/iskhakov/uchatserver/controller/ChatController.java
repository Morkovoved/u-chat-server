package ru.iskhakov.uchatserver.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Controller;
import ru.iskhakov.uchatserver.controller.dto.MessageDto;
import ru.iskhakov.uchatserver.controller.mapper.MessageMapper;
import ru.iskhakov.uchatserver.model.Message;
import ru.iskhakov.uchatserver.model.User;
import ru.iskhakov.uchatserver.service.MessageService;

import java.security.Principal;

@Controller
@Slf4j
@RequiredArgsConstructor
public class ChatController {
    private final SimpMessagingTemplate simpMessagingTemplate;
    private final MessageService messageService;
    private final MessageMapper messageMapper;

    @MessageMapping("/messages/private")
//    @SendTo(PRIVATE_BROKER_MESSAGE_PATH)
    public void sendPrivateMessage(@Payload Message message, Principal principal) {
        log.info("Principal: " + principal.getName() + "Message: " + message);
        Message persistedMessage =  messageService.create(message);
        for(User recipient: message.getRecipients()) {
            simpMessagingTemplate.convertAndSendToUser(recipient.getUsername(), "/queue/message/private", persistedMessage);
        }
    }

    @MessageMapping("/messages/broadcast")
    @SendTo("/topic/message")
    public MessageDto sendBroadcastMessage(@Payload MessageDto messageDto, Principal principal) {
        log.info("Topic Principal: " + principal.getName() + "Topic Message: " + messageDto);
        Message message = messageService.create(messageMapper.mapToEntity(messageDto));
        return messageMapper.mapToDto(message);
    }
}
