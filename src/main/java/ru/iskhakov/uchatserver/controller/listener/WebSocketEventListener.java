package ru.iskhakov.uchatserver.controller.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.messaging.SessionConnectEvent;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import ru.iskhakov.uchatserver.exception.NotAuthorizedException;
import ru.iskhakov.uchatserver.exception.UserNotFoundException;
import ru.iskhakov.uchatserver.service.MessageService;

import javax.transaction.Transactional;
import java.security.Principal;
import java.util.Optional;

@Component
@Slf4j
@RequiredArgsConstructor
public class WebSocketEventListener implements ApplicationListener<SessionConnectEvent> {
    private final MessageService messageService;

    @Override
    public void onApplicationEvent(SessionConnectEvent event) {
        String id = Optional.ofNullable(event.getUser()).map(Principal::getName).orElse("Anonymous");
        StompHeaderAccessor accessor = StompHeaderAccessor.wrap(event.getMessage());
        String userName = Optional.ofNullable(accessor.getUser()).map(Principal::getName).orElse("Anonymous");
        boolean isConnect = accessor.getCommand()== StompCommand.CONNECT;
        boolean isDisconnect = accessor.getCommand()== StompCommand.DISCONNECT;
        log.info("[onApplicationEvent] " + userName);
        log.info("Connect: " + isConnect + ", disconnect: " + isDisconnect + ", event [sessionId: " +
                accessor.getSessionId() + ";" + id + " ,command= " + accessor.getCommand());

    }

    @EventListener
    @Transactional
    public void onSocketConnected(SessionConnectedEvent event) {
        try {
            String username = Optional.ofNullable(event.getUser()).map(Principal::getName).orElseThrow(UserNotFoundException::new);
            log.info("[Connected] " + username);
            messageService.sendConnectMessage(username);
        } catch (Exception e) {
            log.error("Error in WebSocketListener on connecting. ", e);
        }
    }
    @EventListener
    public void onSocketDisconnected(SessionDisconnectEvent event) {
        try {
            String username = Optional.ofNullable(event.getUser()).map(Principal::getName).orElseThrow(UserNotFoundException::new);
            log.info("[Connected] " + username);
            messageService.sendDisconnectMessage(username);
        } catch (Exception e) {
            log.error("Error in WebSocketListener on disconnecting. ", e);
        }
    }

    private String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication == null || !authentication.isAuthenticated()) {
            log.warn("User was not authenticated");
            throw new NotAuthorizedException();
        }
        return ((Principal) authentication.getPrincipal()).getName();
    }
}