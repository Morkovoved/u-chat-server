package ru.iskhakov.uchatserver.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;

@Data
public class UserStateDto {
    @Positive(message = "Identifier must be positive")
    private Integer id;
    @NotEmpty(message = "Please provide a userStateType")
    private String userStateType;
    @Past(message = "The creation date must be less than the current date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private LocalDateTime createdDate;
    @Past(message = "The update date must be less than the current date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private LocalDateTime updatedDate;
}
