package ru.iskhakov.uchatserver.controller.dto;

import lombok.Data;

import javax.validation.constraints.Positive;
import java.util.List;

@Data
public class RoomDto {
    @Positive(message = "Identifier must be positive")
    private Integer id;
    private List<UserDto> admins;
    private List<MessageDto> messages;
}
