package ru.iskhakov.uchatserver.controller.dto;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Positive;

@Data
public class RoleDto {
    @Positive(message = "Identifier must be positive")
    private Integer id;
    @NotEmpty(message = "Please provide a name")
    private String name;
}
