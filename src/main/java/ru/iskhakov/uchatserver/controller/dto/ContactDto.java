package ru.iskhakov.uchatserver.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.Set;

@Data
public class ContactDto {
    @Positive(message = "Identifier must be positive")
    private Integer id;
    @NotEmpty(message = "Please provide a username")
    @Size(min=2, max=50, message = "Not less than two characters and not more than 50")
    private String username;
    private String password;
    @NotEmpty(message = "Please provide a name")
    @Size(min=2, max=50, message = "Not less than two characters and not more than 50")
    private String name;
    @NotEmpty(message = "Please provide a surname")
    @Size(min=2, max=50, message = "Not less than two characters and not more than 50")
    private String surname;
    @Email(message = "Invalid email")
    private String email;
    @Past(message = "The creation date must be less than the current date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
    private LocalDateTime createdDate;
    private Boolean enabled;
    @JsonIgnoreProperties({"user"})
    private UserStateDto userState;
    private Set<RoleDto> roles;
}
