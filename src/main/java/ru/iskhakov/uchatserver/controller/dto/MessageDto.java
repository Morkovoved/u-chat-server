package ru.iskhakov.uchatserver.controller.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Past;
import javax.validation.constraints.Positive;
import java.time.LocalDateTime;
import java.util.List;

@Data
public class MessageDto {
    @Positive(message = "Identifier must be positive")
    private Long id;
    private UserDto sender;
    private List<UserDto> recipients;
    @NotEmpty(message = "Please provide a type")
    private String type;
    @NotEmpty(message = "Please provide a content")
    private String content;
    @Past(message = "The creation date must be less than the current date")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd HH:mm:ss.SSS")
//    @JsonView
    private LocalDateTime createdDate;
}
