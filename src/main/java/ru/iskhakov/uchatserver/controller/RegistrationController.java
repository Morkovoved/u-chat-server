package ru.iskhakov.uchatserver.controller;


import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;
import ru.iskhakov.uchatserver.controller.dto.UserDto;
import ru.iskhakov.uchatserver.controller.mapper.UserMapper;
import ru.iskhakov.uchatserver.exception.UserAlreadyExistException;
import ru.iskhakov.uchatserver.exception.UserNotFoundException;
import ru.iskhakov.uchatserver.model.User;
import ru.iskhakov.uchatserver.service.UserService;

import javax.validation.Valid;
import java.security.Principal;

@RestController
@RequestMapping(value = "/registrations", produces = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
@RequiredArgsConstructor
//@AllArgsConstructor(onConstructor = @__(@Autowired))
public class RegistrationController {
    private final PasswordEncoder passwordEncoder;
    private final UserService userService;
    private final UserMapper userMapper;


    @GetMapping("/authenticate")
    @ResponseStatus(HttpStatus.OK)
    public UserDto authenticate(@RequestParam String username, @RequestParam String password) {
        User user = userService.findByUsername(username);
        if (user != null && user.getPassword().equals(password)) {
            throw new UserNotFoundException("Username or password is incorrect.");
        }
        return userMapper.mapToDto(user);
    }

    @GetMapping("/logout")
    @ResponseStatus(HttpStatus.OK)
    public UserDto logout(Principal principal) {
        User user = userService.findByUsername(principal.getName());
        return userMapper.mapToDto(user);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserDto save(@Valid @RequestBody UserDto userDto) {
        if (userService.existsByUsername(userDto.getUsername())) {
            throw new UserAlreadyExistException("Username already exists.");
        }
        if (userService.existsByEmail(userDto.getEmail())) {
            throw new UserAlreadyExistException("Email already exists.");
        }
        userDto.setPassword(passwordEncoder.encode(userDto.getPassword()));
        User user = userService.create(userMapper.mapToEntity(userDto));
        return userMapper.mapToDto(user);
    }
}
