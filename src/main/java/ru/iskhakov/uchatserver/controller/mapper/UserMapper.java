package ru.iskhakov.uchatserver.controller.mapper;

import org.mapstruct.Mapper;
import ru.iskhakov.uchatserver.controller.dto.UserDto;
import ru.iskhakov.uchatserver.model.User;

@Mapper(componentModel = "spring")
public interface UserMapper extends BaseMapper<User, UserDto> {
}
