package ru.iskhakov.uchatserver.controller.mapper;

import java.util.List;

public interface BaseMapper<E, D> {
    D mapToDto(E entity);
    E mapToEntity(D dto);
    List<D> mapToDtos(List<E> entities);
    List<E> mapToEntities(List<D> dtos);
}
