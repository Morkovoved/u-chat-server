package ru.iskhakov.uchatserver.controller.mapper;

import org.mapstruct.Mapper;
import ru.iskhakov.uchatserver.controller.dto.RoomDto;
import ru.iskhakov.uchatserver.model.Room;

@Mapper(componentModel = "spring")
public interface RoomMapper extends BaseMapper<Room, RoomDto> {
}
