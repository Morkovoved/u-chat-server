package ru.iskhakov.uchatserver.controller.mapper;

import org.mapstruct.Mapper;
import ru.iskhakov.uchatserver.controller.dto.MessageDto;
import ru.iskhakov.uchatserver.model.Message;

@Mapper(componentModel = "spring")
public interface MessageMapper extends BaseMapper<Message, MessageDto> {
//    @Mappings({
//            @Mapping(target="employeeId", source="entity.id"),
//            @Mapping(target="employeeName", source="entity.name")
//    })
//    @Mapping(source = "engine.horsePower", numberFormat = "#.##E0", target = "engine.horsePower")
//    @Mapping(source = "manufacturingDate", dateFormat = "dd.MM.yyyy", target = "manufacturingDate")
//    @Mapping(target="startDt", source="dto.employeeStartDt", dateFormat="dd-MM-yyyy HH:mm:ss")
//    MessageDto toMessageDto(Message message);
//    Message toMessage(MessageDto messageDto);
}
