package ru.iskhakov.uchatserver.controller;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.iskhakov.uchatserver.controller.dto.UserDto;
import ru.iskhakov.uchatserver.controller.mapper.UserMapper;
import ru.iskhakov.uchatserver.model.User;
import ru.iskhakov.uchatserver.service.UserService;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
@Secured({"ROLE_USER", "ROLE_ADMIN"})
@Slf4j
@RequiredArgsConstructor
//@AllArgsConstructor(onConstructor = @__(@Autowired))
//@CrossOrigin(origins = "http://localhost:8080")
public class UserController {
    private final PasswordEncoder passwordEncoder;
    private final UserService userService;
    private final UserMapper userMapper;

    @PutMapping
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public UserDto update(@Valid @RequestBody UserDto userDto) {
        User user = userService.update(userMapper.mapToEntity(userDto));
        return userMapper.mapToDto(user);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Integer id) {
        userService.delete(id);
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto get(@PathVariable Integer id) {
        User user = userService.find(id);
        return userMapper.mapToDto(user);
    }

    @GetMapping("/username/{username}")
    @ResponseStatus(HttpStatus.OK)
    public UserDto getByUsername(@PathVariable String username) {
        User user = userService.findByUsername(username);
        return userMapper.mapToDto(user);
    }

    @GetMapping("/email/{email}")
    public UserDto getByEmail(@PathVariable String email) {
        User user = userService.findByEmail(email);
        return userMapper.mapToDto(user);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<UserDto> getAll() {
        List<User> users = userService.findAll();
        return userMapper.mapToDtos(users);
    }

    @GetMapping("/{id}/contacts")
    public List<UserDto> getContacts(@PathVariable Integer id) {
        List<User> users = userService.findContacts(id);
        return userMapper.mapToDtos(users);
    }

    @PutMapping("/{id}/contacts/{contactId}")
    public UserDto addContact(@PathVariable Integer id, @PathVariable Integer contactId) {
        User user = userService.addContact(id, contactId);
        return userMapper.mapToDto(user);
    }
}
